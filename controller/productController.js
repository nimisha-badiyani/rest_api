import { Product } from '../models';
import multer from 'multer';
import path from 'path';
import CustomErrorHandler from '../services/CustomErrorHandler';
import fs from 'fs';
import Joi from 'joi';
import productSchema from '../validators/productValidator';

const storage = multer.diskStorage({
  destination: (req, file, cb) => cb(null, "uploads/"),
    filename: (req, file, cb) => {
        const uniqueName = `${Date.now()}-${Math.round(
          Math.random() * 1e9
        )}${path.extname(file.originalname)}`;
        cb(null, uniqueName);
  }
});

const handleMultipartData = multer({ storage, limits: { filleSize: 1000000 * 5 } }).single('image'); //5mb

const productController = {
  async store(req, res, next) {
    // Mutlipart from data
    handleMultipartData(req, res, async (err) => {
      if (err) {
        return next(CustomErrorHandler.serverError(err.message));
      }
      const filePath = req.file.path;
      // validation
      const { error } = productSchema.validate(req.body);
      if (error) {
        // delete the uploaded file
        fs.unlink(`${appRoot}/${filePath}`, (err) => {
          if (err) {
            return next(CustomErrorHandler.serverError(err.message));
          }
        });


        return next(error);
      }

      const { name, price, size } = req.body;
      let documents;

      try {
        documents = await Product.create({
          name, price, size, image: filePath
        })
      } catch (err) {
        return next(err);
      }

      res.status(201).json({ documents });
    })
  },
  update(req, res, next) {
    handleMultipartData(req, res, async (err) => {
      if (err) {
        return next(CustomErrorHandler.serverError(err.message));
      }
      let fillePath;
      if (req.file) {
        const filePath = req.file.path;
      }
           
      // validation
      const { error } = productSchema.validate(req.body);
      if (error) {
        // delete the uploaded file
        if (req.file) {
          fs.unlink(`${appRoot}/${filePath}`, (err) => {
            if (err) {
              return next(CustomErrorHandler.serverError(err.message));
            }
          });
        }

        return next(error);
      }

      const { name, price, size } = req.body;
      let documents;

      try {
        documents = await Product.findOneAndUpdate({ _id: req.params.id }, {
          name,
          price,
          size,
          ...(req.file && { image: filePath })
        }, { new: true });
        //  console.log(documents);
      } catch (err) {
        return next(err);
      }

      res.status(201).json({ documents });
    });
  },
  async destroy(req, res, next) {
    const documents = await Product.findOneAndRemove({ _id: req.params.id }, { _id: req.params.id });
    if (!documents) {
      return next(new Error('Nothing to delete'));
    }
    // image delete
    const imagePath = documents._doc.image;
    fs.unlink(`${appRoot}/${imagePath}`, (err) => {
      if (err) {
        return next(CustomErrorHandler.serverError())
      }
      return res.json(documents);
    });
  },

  async index(req, res, next) {
    let documents;
    // pagination
    try {
        documents = await Product.find().select('-updatedAt -__v').sort({_id: -1});
    } catch (err) {
        return next(CustomErrorHandler.serverError());
    }
    return res.json(documents);
  },
  async show(req, res, next) {
    let documents;
    try {
      documents = await Product.findOne({ _id: req.params.id }).select(
        "-updatedAt -__v"
      );
    } catch (err) {
      return next (CustomErrorHandler.serverError());
    }
    return res.json(documents);
  }
}

export default productController;