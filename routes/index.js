import express from 'express';
const router = express.Router();
import {
  userController,
  refreshController,
  productController,
  registerController,
  LoginController} from "../controller";
import auth from '../middleware/auth';
import admin from '../middleware/admin';


router.post('/register', registerController.register);
router.post('/Login', LoginController.Login);
router.get('/me', auth, userController.me);
router.post('/refresh', refreshController.refresh);
router.post('/logout', auth, LoginController.logout);

router.post('/products',[auth , admin], productController.store);
router.put('/products/:id', [auth, admin], productController.update);
router.delete('/products/:id', [auth, admin], productController.destroy);
router.get('/products', productController.index);
router.get('/products/:id', productController.show);

export default router;